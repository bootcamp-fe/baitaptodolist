const BASE_URL = "https://62db6ca2e56f6d82a77284cd.mockapi.io";

let dataArr = [];
let sortedArr = [];
let reversedArr = [];
let sortedByTimeArr = [];

function getToDoList(isSpinner=true){
    if(isSpinner){
        turnOnLoading();
    }
    axios({
        url:`${BASE_URL}/todo`,
        method:"GET",
    }).then((res)=>{
        dataArr = res.data;
        sortedByTimeArr = dataArr.sort((a, b) => (a.id).localeCompare((b.id))).slice();
        renderToDoList(sortedByTimeArr);
        turnOffLoading();
        sortedArr = dataArr.sort((a, b) => (a.task).localeCompare((b.task))).slice();
        reversedArr = dataArr.reverse((a, b) => (a.task).localeCompare((b.task))).slice();
    }).catch((err)=>{
        console.log(err);
        turnOffLoading();
    });
};

getToDoList();

function renderToDoList(data){
    let haveYetFinishListHtml = "";
    let alreadyFinishListHtml = "";
    data.forEach((item)=>{
        if(!item.done){
            let itemHTML=/*HTML*/`
            <li class="todo-item">
                <div class="todo-text">${item.task}</div>
                <div class="todo-btn">
                  <i data-id=${item.id} onclick=deleteItem(${item.id}) class="far fa-trash-alt"></i>
                  <i data-check-id=${item.id} onclick=markAsFinish(${item.id}) class="far fa-check-circle"></i>
                </div>
              </li>
            `;
            haveYetFinishListHtml+=itemHTML;
        } else {
            let itemHTML=/*HTML*/`
            <li class="todo-item">
                <div class="todo-text">${item.task}</div>
                <div class="todo-btn">
                  <i data-id=${item.id} onclick=deleteItem(${item.id}) class="far fa-trash-alt"></i>
                  <i data-check-id=${item.id} onclick=markAsUnfinished(${item.id}) class="far fa-check-circle"></i>
                </div>
              </li>
            `;
            alreadyFinishListHtml+=itemHTML;
        }
    });
    document.getElementById("todo").innerHTML=haveYetFinishListHtml;
    document.getElementById("completed").innerHTML=alreadyFinishListHtml;
};

function deleteItem(id){
    turnOnSpinner(id,"id");
    // Disable function onclick detele and check btn
    document.querySelector(`i[data-id="${id}"]`).onclick = null;
    document.querySelector(`i[data-check-id="${id}"]`).onclick = null;
    axios({
        url: `${BASE_URL}/todo/${id}`,
        method:"DELETE",
    }).then(function(res){
        getToDoList(false);
    }).catch(function(err){
        console.log(err);
        document.querySelector(`i[data-id="${id}"]`).onclick = `deleteItem(${id})`;
        document.querySelector(`i[data-check-id="${id}"]`).onclick = `markAsUnfinished(${id})`;
        turnOffSpinnerForDelete(id,"id");
    });
};

function markAsFinish(id){
    turnOnSpinner(id,"check-id");
    let finishTask = dataArr.find((item)=>{
        return item.id == id;
    });
    finishTask.done = true;
    axios({
        url:`${BASE_URL}/todo/${id}`,
        method:"PUT",
        data:finishTask,
    }).then((res)=>{
        turnOffSpinnerForMark(id,"check-id");
        getToDoList(false);
    }).catch((err)=>{
        turnOffSpinnerForMark(id,"check-id");
        console.log(err);
    });
};

function markAsUnfinished(id){
    turnOnSpinner(id,"check-id");
    let unfinishedTask = dataArr.find((item)=>{
        return item.id == id;
    });
    unfinishedTask.done = false;
    axios({
        url:`${BASE_URL}/todo/${id}`,
        method:"PUT",
        data:unfinishedTask,
    }).then((res)=>{
        turnOffSpinnerForMark(id,"check-id");
        getToDoList(false);
    }).catch((err)=>{
        turnOffSpinnerForMark(id,"check-id");
        console.log(err);
    });
}

// Add Item Function
document.getElementById("addItem").addEventListener("click",addItem);
function addItem(){
    let taskContent = document.getElementById("newTask").value;
    let newTask = {
        task:`${taskContent}`,
        done:false,
    }
    if(taskContent!=""){
        turnOnSpinnerForAddItem();
        axios({
            url:`${BASE_URL}/todo`,
            method:"POST",
            data:newTask,
        }).then(function(res){
            turnOffSpinnerForAddItem();
            getToDoList(false);
        }).catch(function(err){
            turnOffSpinnerForAddItem();
            console.log(err);
        });
    }
}


// Form Controller

function turnOnSpinner(id,dataTag){
   document.querySelector(`i[data-${dataTag}="${id}"]`).classList.value = "spinner-border spinner-border-sm" ;
};

function turnOffSpinnerForDelete(id,dataTag){
    document.querySelector(`i[data-${dataTag}="${id}"]`).classList.value = "far fa-trash-alt" ;
 };

 function turnOffSpinnerForMark(id,dataTag){
    document.querySelector(`i[data-${dataTag}="${id}"]`).classList.value = "far fa-check-circle" ;
 };

function turnOnLoading(){
    document.querySelector(".loading").style.display = "flex";
};

function turnOffLoading(){
    document.querySelector(".loading").style.display = "none";
};

function turnOnSpinnerForAddItem(){
    document.querySelector("#addItem").innerHTML = `<div class="spinner-grow text-danger"></div>`;
};

function turnOffSpinnerForAddItem(){
    document.querySelector("#addItem").innerHTML = `<i class="fa fa-plus"></i>`;
    document.getElementById("newTask").value = "";
}

// Sort Function

document.querySelector(".filter-btn #two").addEventListener("click",renderSortedArr);
document.querySelector(".filter-btn #three").addEventListener("click",renderReversedArr);
document.querySelector(".filter-btn #all").addEventListener("click",renderByAddingTime);

function renderSortedArr(){
    renderToDoList(sortedArr);
}

function renderReversedArr(){
    renderToDoList(reversedArr);
}

function renderByAddingTime(){
    renderToDoList(sortedByTimeArr);
}
